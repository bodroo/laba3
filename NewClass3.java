/*
 + * Создать двумерный массив из 7 строк по 4 столбца в каждой из случайных целых
 + * чисел из отрезка [-5;5]. Вывести массив на экран. Определить и вывести на экран 
 + * индекс строки с наибольшим по модулю произведением элементов. Если таких строк 
 + * несколько, то вывести индекс первой встретившейся из них.
 + */


/**
 *
 * @author mei1142b
 */
public class NewClass3 {

    public static void main(String[] args) {
        int max = 0, k = 1, ind = 0;
        int[][] a;
        a = new int[7][4];
        /*Инициализация двумерного массива*/
        for (int i = 0; i < 7; i++) {
            System.out.println();

            for (int j = 0; j < 4; j++) {
                a[i][j] = (int) (Math.random() * 11 - 5);
                System.out.print(a[i][j] + " ");
                k = k * a[i][j];
            }
            /*Подсчет произведения в строках и запоминание индекса*/
            if (Math.abs(k) >= max) {
                max = Math.abs(k);
                ind = i;
            }
            k = 1;
        }

        System.out.println();
        System.out.println(max + " " + (ind + 1));

    }
}
