/*
 * Создать двумерный массив из 8 строк по 5 столбцов в каждой из случайных целых 
 * чисел из отрезка [10;99]. 
 * Вывести массив на экран.
 */


/**
 *
 * @author mei1142b
 */
public class NewClass2 {
    public static void main(String[] args) {
        int[][] a;
      a = new int[8][5];
        /*Инициализация двумерного массива*/
        for(int i=0;i<8;i++){
            System.out.println();
            for(int j=0;j<5;j++){
               a[i][j]=(int) (Math.random()*90+10);
                System.out.print(a[i][j]+ " ");

            }

        }

    }

}
