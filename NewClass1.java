/*
+ * Создайте массив из 15 случайных целых чисел из отрезка [0;9]. 
+ * Выведите массив на экран. 
+ * Подсчитайте сколько в массиве чётных элементов и 
+ * выведете это количество на экран на отдельной строке.
+ */



/**
+ *
+ * @author Елизавета
+ */

public class NewClass1 {

    public static void main(String[] args) {
        int k = 0;
        int[] a;
        a = new int[15];
        /*Инициализация массива*/
       for(int i=0;i<=14;i++){
           a[i] = (int) (Math.random()*10);
           /*Вывод массива и подсчет четных чисел*/
           System.out.print(a[i] + " ");
           if(a[i]%2 == 0) k+=1;
        }
        System.out.println();
        System.out.print(k);

    }

}

